import os
import io
import glob
import struct
from threading import Thread, Lock
from time import sleep
from typing import Dict

import inputs
from inputs import devices, GamePad, InputEvent

#00:01:6C:E1:70:70
#cat "/sys/class/power_supply/sony_controller_battery_00:01:6C:E1:70:70/capacity"

BTN_MAP_X360: dict = {
    'Key': (
        ('ABS_HAT0X_NEG', 'Left'),
        ('ABS_HAT0X_POS', 'Right'),
        ('ABS_HAT0Y_NEG', 'Up'),
        ('ABS_HAT0Y_POS', 'Down'),
        ('BTN_WEST', 'Square'),
        ('BTN_EAST', 'Circle'),
        ('BTN_NORTH', 'Triangle'),
        ('BTN_SOUTH', 'Cross'),
        ('BTN_START', 'Select'),
        ('BTN_SELECT', 'Start'),
        ('BTN_TL', 'L1'),
        ('BTN_TR', 'R1'),
        ('BTN_THUMBL', 'L3'),
        ('BTN_THUMBR', 'R3')
    ),
    'Absolute': (
        ('ABS_X', 'ABS_X'),
        ('ABS_Y', 'ABS_Y'),
        ('ABS_Z', 'ABS_Z'),
        ('ABS_RX', 'ABS_RX'),
        ('ABS_RY', 'ABS_RY'),
        ('ABS_RZ', 'ABS_RZ'),
        ('ABS_HAT0X', 'ABS_HAT0X'),
        ('ABS_HAT0Y', 'ABS_HAT0Y')
    )
}

BTN_MAP_PS4: dict = {
    'Key': (
        ('ABS_HAT0X_NEG', 'Left'),
        ('ABS_HAT0X_POS', 'Right'),
        ('ABS_HAT0Y_NEG', 'Up'),
        ('ABS_HAT0Y_POS', 'Down'),
        (0x00, 'Cross'),
        (0x01, 'Circle'),
        (0x02, 'Triangle'),
        (0x03, 'Square'),
        (0x04, 'L1'),
        (0x05, 'R1'),
        (0x06, 'L2'),
        (0x07, 'R2'),
        (0x08, 'Select'),
        (0x09, 'Start'),
        (0x0A, 'Home'),
        (0x0B, 'L3'),
        (0x0C, 'R3')
    ),
    'Absolute': (
        (0x00, 'ABS_X'),
        (0x01, 'ABS_Y'),
        (0x02, 'ABS_Z'),
        (0x03, 'ABS_RX'),
        (0x04, 'ABS_RY'),
        (0x05, 'ABS_RZ')
    ),
    'Relative': (
        (0x00, 'REL_X'),
        (0x01, 'REL_Y'),
        (0x02, 'REL_Z'),
        (0x03, 'REL_RX'),
        (0x04, 'REL_RY'),
        (0x05, 'REL_RZ'),
        (0x06, 'ABS_HAT0X'),
        (0x07, 'ABS_HAT0Y')
    )
}

class PS4_Controller(GamePad):
    EVENT_TYPE = str("<LhBB")

    def _get_path_infomation(self):
        """Get useful infomation from the device path."""
        return ("bluetooth", self._device_path, "joystick")

    def _do_iter(self):
        read_size = struct.calcsize(self.EVENT_TYPE)
        data = self._get_data(read_size)
        if not data:
            return None
        evdev_objects = struct.iter_unpack(self.EVENT_TYPE, data)
        events = [self._make_event(*event) for event in evdev_objects]
        return [e for e in events if e]

    def _make_event(self, tv_sec, value, ev_type, code):
        """Create a friendly Python object from an evdev style event."""
        try:
            event_type = self.manager.get_event_type(ev_type)
            eventinfo = {
                "ev_type": event_type,
                "state": value,
                "timestamp": tv_sec,
                "code": code
            }
            return InputEvent(self, eventinfo)

        except:
            print(f"Event: {ev_type}, {code}={value}")

    
    def _set_name(self):
        pass

class joystick():
    """The joystick object listens to the gamepad events"""

    stick_left: list = [0, 0]
    """Left stick (X,Y) coordinate"""

    stick_right: list = [0, 0]
    """Right stick (X,Y) coordinate"""

    trigger_left: int = 0
    """Left analog trigger value"""

    trigger_right: int = 0
    """Right analog trigger value"""

    buttons: Dict[str, bool] = {}
    """Dictionary of button states"""

    debug = True

    def __init__(self, id=None):
        """id = The index of the connected HID device to listen to for events. When None, the first available gamepad will be used."""
        if id == None:
            if os.name == 'posix':
                self.device = PS4_Controller(devices, "/dev/input/js0", "/dev/input/js0")
                self.ledID = glob.glob("/sys/class/leds/*:global")[0].replace(':global', '')
            else:
                self.device = devices.gamepads[0]
        else:
            self.device = devices[id]

        self.button_map = BTN_MAP_X360 if str(self.device).startswith("Microsoft") else BTN_MAP_PS4
        print(self.device)

        for btn in self.button_map['Key']:
            if type(btn) == tuple:
                self.buttons[btn[1]] = False
            else:
                self.buttons[btn] = False

        self.lock = Lock()
        
        update_thread = Thread(name="JoystickListener",target=self._run)
        update_thread.daemon = True
        update_thread.start()

    def _run(self):
        while True:
            try:
                events = self.device.read()
            except EOFError:
                events = []
            except:
                continue

            for event in [e for e in events if e]:
                if(self.handle_event(event)):
                    # self.print()
                    pass

    def set_rgb(self, r=0, g=0, b=0):
        if os.name == 'posix':
            with open(f'{self.ledID}:red/brightness', "w") as myfile:
                myfile.write(str(r))
            with open(f'{self.ledID}:green/brightness', "w") as myfile:
                myfile.write(str(g))
            with open(f'{self.ledID}:blue/brightness', "w") as myfile:
                myfile.write(str(b))

    def print(self):
        """Print the """
        print(f"Left: ({self.stick_left[0]:6}, {self.stick_left[1]:6}), Right: ({self.stick_right[0]:6},{self.stick_right[1]:6}) {self.get_pressed()}")

    def get_pressed(self):
        return "".join((btn if state else "") for btn, state in self.buttons.items())

    def handle_event(self, event):
        with self.lock:
            if event.ev_type == 'Absolute':
                code = dict(self.button_map['Absolute'])[event.code]
                if code == 'ABS_X':
                    self.stick_left[0] = event.state
                elif code == 'ABS_Y':
                    self.stick_left[1] = event.state
                elif code == 'ABS_RX':
                    self.stick_right[0] = event.state
                elif code == 'ABS_RY':
                    self.stick_right[1] = event.state
                elif code == 'ABS_Z':
                    self.trigger_left = event.state
                elif code == 'ABS_RZ':
                    self.trigger_right = event.state
                elif code == 'ABS_HAT0X':
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0X_NEG']] = (event.state < 0)
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0X_POS']] = (event.state > 0)
                    return 1
                elif code == 'ABS_HAT0Y':
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0Y_NEG']] = (event.state < 0)
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0Y_POS']] = (event.state > 0)
                    return 1
                else:
                    print("Absolute not mapped: ", event.code)
            if event.ev_type == 'Relative':
                code = dict(self.button_map['Relative'])[event.code]
                if code == 'REL_X':
                    self.stick_left[0] = event.state
                elif code == 'REL_Y':
                    self.stick_left[1] = -event.state
                elif code == 'REL_RX':
                    self.stick_right[0] = event.state
                elif code == 'REL_RY':
                    self.stick_right[1] = -event.state
                elif code == 'REL_Z':
                    self.trigger_left = event.state
                elif code == 'REL_RZ':
                    self.trigger_right = event.state
                elif code == 'ABS_HAT0X':
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0X_NEG']] = (event.state < 0)
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0X_POS']] = (event.state > 0)
                    return 1
                elif code == 'ABS_HAT0Y':
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0Y_NEG']] = (event.state < 0)
                    self.buttons[dict(self.button_map['Key'])['ABS_HAT0Y_POS']] = (event.state > 0)
                    return 1
                else:
                    print("Relative not mapped: ", event.code)
            elif event.ev_type == 'Key':
                try:
                    self.buttons[dict(self.button_map['Key'])[event.code]] = event.state
                    return 1
                except:
                    pass
            elif event.ev_type != 'Sync' and event.ev_type != 'Misc':
                pass
                # print(event.ev_type, event.code, event.state)