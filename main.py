#!/usr/bin/env python

from collections import namedtuple
from datetime import datetime, timedelta
from genericpath import exists

import os
from inputs import devices
from serial import Serial
import rich
from rich.console import Console, Group
from rich.live import Live
from rich.progress import Progress
from rich.progress_bar import ProgressBar

from joystick import joystick
from manual import manual_drive, mission, line

UserInput = namedtuple('UserInput', 'task timeout condition result')

if os.name == 'posix':
    cmd = Serial(port="/dev/ttyACM0",
            baudrate=115200, timeout=0, stopbits=1)
else:
    cmd = Serial()

modes = {
    "Arm":  UserInput("Arming...", 2000, lambda j: j.buttons['L1'] and j.buttons['R1'], False),
    "Manual":  UserInput("Enabling manual drive...", 1000, lambda j: j.buttons['L3'] and j.buttons['R3'], False),
    "Exit": UserInput("Exiting...", 1000, lambda j: j.buttons['Select'] and j.buttons['Start'], False),
    "Test": UserInput(None, None, lambda j: j.buttons['Cross'], False),
    "Square": UserInput("Starting mission", 1000, lambda j: j.buttons['Square'], False),
    "Circle": UserInput("Starting mission", 1000, lambda j: j.buttons['Circle'], False)
}

def prompt_hid() -> int:
    """Display all connected HID devices to the user and request them to make a selection what controller to use."""
    joysticks = devices.all_devices
    for i, stick in enumerate(joysticks):
        print(f"{i:>2}) {stick}")
    return int(input("Use Joystick: "))

def check_for_input(joy, mode):
    try:
        with Progress() as prog:
            timer = prog.add_task(mode.task, total=mode.timeout)
            start_time = now = datetime.now()
            while not prog.finished:
                last = now
                now = datetime.now()
                prog.update(timer, advance=(now-last).total_seconds() * 1e3)

                #buttons have been released
                if not (mode.condition(joy)):
                    raise InterruptedError("Key released")
            return True
    
    except InterruptedError:
        return False

    except Exception as e:
        print(type(e))
        return False
        
    finally:
        rich.print(f"Timer expired in [bold cyan]{now-start_time}[/bold cyan]")

def test(joy):
    def generate_progress() -> Group:
        from rich.progress import ( 
        BarColumn,
        Progress,
        TaskID,
        TextColumn,
        )
        group = Group()

        bars = BarColumn()
        progress = Progress(TextColumn("[bold blue]{task.fields[filename]}"), bars)

        values = [
            joy.stick_left[0] + 32767,
            joy.stick_left[1] + 32767,
            joy.stick_right[0] + 32767,
            joy.stick_right[1] + 32767
        ]
        # bars = [Bar(size=65535, begin=0, end=val, color="red") for val in values]
        [progress.add_task("Val", total=65535, completed=val) for val in values]
        group.renderables.extend([bars.render(task) for task in progress.tasks])
        return group

    with Live(generate_progress(), refresh_per_second=10) as live:
        done = False
        while not done:
            live.update(generate_progress())
            if joy.buttons['Select'] and joy.buttons['Start']:
                done = True

def main(joystick: joystick) -> None:
    """The main loop of the code"""
    done = False    #when True, main loop will break.
    armed = False

    with console.status("[bold green]Waiting for user input...", spinner='line') as status:
        while not done:
            for key, mode in modes.items():
                status.start()
                if mode.condition(joystick):
                    if key == 'Exit':
                        status.update("[bold green]Waiting for user input...")
                        status.update(spinner='line')
                        joystick.set_rgb(g=100)
                        armed = False
                        cmd.write(b'Stop')
                        status.stop()
                        if check_for_input(joystick, mode):
                            done = True
                            armed = False
                            break
                        continue

                    if key == 'Arm':
                        if armed:
                            continue
                        status.stop()
                        if check_for_input(joystick,mode):
                            status.update("[bold red]System armed...[/]")
                            status.update(spinner='toggle3')
                            joystick.set_rgb(r=100)
                            armed = True
                        continue
                    
                    if key == 'Manual':
                        if not armed:
                            continue
                        status.stop()
                        if check_for_input(joystick, mode):
                            status.update("[bold red]Manual drive anabled...[/]")
                            status.update(spinner='arrow3')
                            joystick.set_rgb(b=100)
                            manual_drive(joystick)
                            status.update("[bold green]Waiting for user input...")
                            status.update(spinner='line')
                            joystick.set_rgb(g=100)
                            armed = False
                        continue

                    if key == 'Test':
                        status.stop()
                        joystick.set_rgb(0xdd, 0x85, 0x00)
                        test(joystick)
                        continue

                    if key == 'Square':
                        if not armed:
                            continue
                        status.stop()
                        if check_for_input(joystick, mode):
                            status.update("[bold red]Running mission...[/]")
                            status.update(spinner='arrow3')
                            mission()

                    if key == 'Circle':
                        if not armed:
                            continue
                        status.stop()
                        if check_for_input(joystick, mode):
                            status.update("[bold red]Running mission...[/]")
                            status.update(spinner='arrow3')
                            line()

                    if key == 'Debug':
                        joystick.debug = not joystick.debug
            

if __name__ == "__main__":
    import sys
    try:
        arg = sys.argv[1]
        if arg == "Select":
            stick = joystick(prompt_hid())
    except:
        stick = joystick()

    console = Console()
    stick.set_rgb(g=64)
    print("Starting main loop")
    main(stick)             #Start the main loop
    stick.set_rgb(64, 64, 64)