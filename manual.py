import os
import codecs
import struct
import time
from serial import Serial

limit = lambda a, _min, _max: min(max(_min, a), _max)

def serial_init():
    if os.name == 'posix':
        return Serial(port="/dev/ttyACM1",
            baudrate=115200, timeout=0, stopbits=1)

def manual_drive(joy): 
    done = False

    s = serial_init()
    
    while not done:
        #do stuff
        deadzone = 20
        x, y = joy.stick_left
        rot, _ = joy.stick_right
        frontleft =  limit(y + rot + x, -32000, 32000)
        rearleft =   limit(y + rot - x, -32000, 32000)
        frontright = limit(y - rot - x, -32000, 32000)
        rearright =  limit(y - rot + x, -32000, 32000)
        
        if os.name == 'posix':
            for motor in [rearleft, rearright, frontleft, frontright]:
                s.write((motor >> 8).to_bytes(1, 'big', signed=True))  

            if s.in_waiting >= 4:
                print(f"{int.from_bytes(s.read(4), byteorder='big', signed=True)}\n")
                s.reset_input_buffer()

        else:
            for motor in [rearleft, rearright, frontleft, frontright]:
                print(motor >> 8, end=', ')
            print("\b\b")


        if joy.buttons['Select'] and joy.buttons['Start']:
            done = True

def mission():
    s = serial_init()

    msg = bytearray([100, 0, 0, 0])
    print(msg)
    print(" ".join(hex(n) for n in msg))
    s.write(msg)
    time.sleep(1)
    if s.in_waiting >= 4:
        print(f"Return: {' '.join(hex(n) for n in list(s.read(4)))}")
        #s.reset_input_buffer()

    msg = bytearray([0, 100, 0, 0])
    print(" ".join(hex(n) for n in msg))
    s.write(msg)
    time.sleep(1)
    if s.in_waiting >= 4:
        print(f"Return: {' '.join(hex(n) for n in list(s.read(4)))}")
        #s.reset_input_buffer()

    msg = bytearray([0, 0, 100, 0])
    print(" ".join(hex(n) for n in msg))
    s.write(msg)
    time.sleep(1)
    if s.in_waiting >= 4:
        print(f"Return: {' '.join(hex(n) for n in list(s.read(4)))}")
        #s.reset_input_buffer()

    msg = bytearray([0, 0, 0, 100])
    print(" ".join([hex(n) for n in msg]))
    s.write(msg)
    time.sleep(1)
    if s.in_waiting >= 4:
        print(f"Return: {' '.join(hex(n) for n in list(s.read(4)))}")
        #s.reset_input_buffer()
    
    msg = bytearray([0, 0, 0, 0])
    print(" ".join([hex(n) for n in msg]))
    s.write(msg)
    time.sleep(1)
    if s.in_waiting >= 4:
        print(f"Return: {' '.join(hex(n) for n in list(s.read(4)))}")

def line():
    s = serial_init()

    print("Starting drive")
    msg = bytearray([40, 40, 40, 40])
    s.write(msg)

    print("Driving")
    time.sleep(7.5)
    
    print("Stop drive")
    msg = bytearray([0, 0, 0, 0])
    s.write(msg)